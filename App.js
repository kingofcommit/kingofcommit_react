import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, FlatList, TextInput } from 'react-native';
import { List, ListItem, ButtonGroup, Icon, Button } from 'react-native-elements';
import { StackNavigator, SwitchNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import LoginScreen from './src/login';
import RegisterScreen from './src/register';
import MainScreen from './src/main';
import RankScreen from './src/rank';
import ChatScreen from './src/chat';

const StackLogin = StackNavigator({
    Login: { screen: LoginScreen },
    Register: { screen: RegisterScreen },
}, {
        headerMode: 'none'
    });

const MainStack = TabNavigator({
    Main: { screen: MainScreen },
    Rank: { screen: RankScreen },
    Chat: { screen: ChatScreen },
},
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Main') {
                    iconName = `ios-home`;
                } else if (routeName == 'Rank') {
                    iconName = `ios-trophy`;
                }
                else {
                    iconName = `ios-chatboxes`;
                }
                return <Ionicons name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: '#00BFFF',
            inactiveTintColor: '#fff',
            lazyLoad: true,
            upperCaseLabel: false,
            indicatorStyle: {
                backgroundColor: 'transparent'
            },
            style: {
                backgroundColor: 'rgba(22, 22, 22, 0.3)',
                borderTopWidth: 0,
                position: 'absolute',
                left: 0,
                right: 0,
                bottom: 0
            }
        },
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        animationEnabled: false,
        swipeEnabled: false,
    });

const RootSwitch = (signedIn = false) => {
    return SwitchNavigator(
        {
            MainStack: { screen: MainStack },
            StackLogin: { screen: StackLogin },
        },
        {
            initialRouteName: signedIn ? "Main" : "StackLogin"
        }
    );
};

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signedIn: false,
            checkedSignIn: false
        };
    }


    render() {
        const { checkedSignIn, signedIn } = this.state;

        const Layout = RootSwitch(signedIn);
        return (
            <Layout />
        );
    }
}
