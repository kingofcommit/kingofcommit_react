import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, FlatList, TextInput, Alert, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';
import { List, ListItem, ButtonGroup, Icon, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';


export default class MainScreen extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            todoTasks: [],
            icon: 'bolt',
            typeIcon: 'font-awesome',
            loading: false,
            selectedIndex: 0,
            background: 'yellow',
            disabled: false,
            user: null,
            visible: false,
            uid: ''
        });
        this.updateIndex = this.updateIndex.bind(this);
        this.ref = firebase.firestore()
    }

    setBackground() {
        this.ref.collection('variables').doc('SjrZMFbVQp0FbWdfLGI2').onSnapshot(function (doc) {
            const { user } = this.state;
            let background = doc.data()['background']
            let disabled = !doc.data()['disabled']
            let visible = doc.data()['visible']
            if (visible == true) {
                if (user) {
                    if (user.uid == doc.data()['uid']) {
                        visible = true
                        disabled = true
                    } else {
                        visible = false
                    }
                }
            }
            this.setState({
                background,
                disabled,
                visible
            })
        }.bind(this));
    }

    componentDidMount() {
        this.setState({ loading: true });
        this.setState({ user: firebase.auth().currentUser });
        this.setBackground();
        this.unsubscribe = this.ref.collection('todoTasks').onSnapshot((querySnapshot) => {
            const todos = [];
            querySnapshot.forEach((doc) => {
                todos.push({
                    user: doc.data().user,
                    icon: doc.data().icon,
                    typeIcon: doc.data().typeIcon,
                    id: doc.data().id
                });
            });
            this.setState({
                todoTasks: todos.sort((a, b) => {
                    return (a.id < b.id);
                }),
                loading: false,
            });
        });
    }

    onPressAdd = () => {
        const { user, icon, typeIcon, todoTasks } = this.state;
        this.setState({ loading: true });
        let id = todoTasks.length + 1
        let count = 1;
        if (user != null || icon != '' || typeIcon != '') {
            this.ref.collection('todoTasks').add({
                user: { email: user.email, uid: user.uid },
                icon,
                typeIcon,
                id,
            }).then((data) => {
                let background = '';
                if (icon == 'bolt') {
                    background = 'bolt'
                } else {
                    background = 'brandao'
                }
                this.ref.collection('rank').where("uid", "==", user.uid)
                    .get()
                    .then((querySnapshot) => {
                        querySnapshot.forEach((doc) => {
                            count = doc.data().count + 1
                            this.ref.collection('rank').doc(doc.id).set({ user: user.email, uid: user.uid, count: count })
                        });
                        console.log(querySnapshot);
                        if (querySnapshot.size == 0) { this.ref.collection('rank').add({ user: user.email, uid: user.uid, count: count }) };

                    });
                this.ref.collection('variables').doc('SjrZMFbVQp0FbWdfLGI2').set({ background, disabled: true, uid: user.uid, visible: true })
                this.setState({
                    uid: '',
                    icon: '',
                    typeIcon: '',
                    loading: false,
                    background,
                    disabled: true
                });
            }).catch((error) => {
                this.setState({ loading: false })
            });
        } else {
            this.setState({ loading: false });
        }
    }

    updateIndex = (selectedIndex) => {
        let icon = ''
        let typeIcon = ''
        if (selectedIndex == 0) {
            icon = 'bolt'
            typeIcon = 'font-awesome'
        } else if (selectedIndex == 1) {
            icon = 'ios-flame'
            typeIcon = 'ionicon'
        } else {
            icon = 'skull'
            typeIcon = 'foundation'
        }
        this.setState({ selectedIndex, icon, typeIcon })
    }

    stopCagada = () => {
        this.setState({ loading: true });
        this.ref.collection('variables').doc('SjrZMFbVQp0FbWdfLGI2').set({ background: 'green', disabled: false })
            .then(() => { this.setState({ loading: false }); })
            .catch(() => { this.setState({ loading: false }); })
    }

    render() {
        const component1 = () => <Icon name='bolt' type='font-awesome' color={selectedIndex == 0 ? 'red' : 'black'} />
        const component2 = () => <Icon name='ios-flame' type='ionicon' color={selectedIndex == 1 ? 'red' : 'black'} />
        const component3 = () => <Icon name='skull' type='foundation' color={selectedIndex == 2 ? 'red' : 'black'} />
        const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }]
        const { selectedIndex } = this.state
        let background = ['#228B22', '#3CB371'];
        let colorItems = '#3CB371';
        let colorText = '#fff';
        if (this.state.background == 'bolt') {
            background = ['#FFD700', '#EEEE00']
            colorItems = '#FFF68F';
            colorText = '#000';
        } else if (this.state.background == 'brandao') {
            background = ['#FF0000', '#FF4500']
            colorItems = '#EE4000';
        }
        return (
            <LinearGradient colors={background} style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                padding: 15
            }}>
                <Spinner visible={this.state.loading} textContent={"Organizando a cagada..."} textStyle={{ color: '#FFF' }} />
                {this.state.disabled ?
                    <Icon
                        raised
                        name={this.state.visible ? 'stop' : 'play'}
                        type='font-awesome'
                        color='#f50'
                        onPress={this.state.visible ? this.stopCagada.bind(this) : this.onPressAdd.bind(this)} /> : null}

                <ButtonGroup
                    onPress={this.updateIndex.bind(this)}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    containerStyle={styles.buttonGroup}
                />
                <List containerStyle={{ marginBottom: 10, backgroundColor: 'transparent', padding: 10, width: '90%', height: '63%', borderTopWidth: 0 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {
                            this.state.todoTasks.map((item, i) => (
                                < View style={{ flexDirection: 'row', width: '100%', backgroundColor: colorItems, margin: 5, alignItems: 'center', justifyContent: 'space-around', padding: 7 }} key={i}>
                                    <Icon
                                        name={item.icon} type={item.typeIcon} color={colorText} containerStyle={{ width: '30%' }} />
                                    <Text style={{ color: colorText, width: '70%' }}>{item.user.email}{item.typeIcon}</Text>
                                </View>
                            ))
                        }
                    </ScrollView>
                </List>
            </LinearGradient >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: '100%'
    },
    button: {
        height: 45,
        backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    buttonGroup: {
        height: 45,
    },
    textinput: {
        height: 45,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        borderColor: '#EEE',
        borderWidth: 1,
        paddingHorizontal: 20,
        marginBottom: 10
    },
    item: {
        backgroundColor: 'rgba(0, 191, 255, 0.8)',
        color: '#fff',
        width: '90%',
        height: 60,
        marginVertical: 5,
        // marginHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    flatstyle: {
        marginVertical: 40,
        backgroundColor: '#069',
    }
});
