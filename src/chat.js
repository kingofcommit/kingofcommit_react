import React, { Component } from 'react';
import { Platform, StyleSheet, ScrollView, Text, View, TouchableOpacity, FlatList, TextInput, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { List, ListItem, ButtonGroup, Icon, Button } from 'react-native-elements';

export default class ChatScreen extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            chatMessages: [],
            loading: false,
            disabled: false,
            visible: false,
            msg: '',
            user: null,
        });
        this.ref = firebase.firestore()
    }

    componentDidMount() {
        this.setState({ loading: true });
        this.setState({ user: firebase.auth().currentUser });
        this.unsubscribe = this.ref.collection('chat').onSnapshot((querySnapshot) => {
            const messages = [];
            querySnapshot.forEach((doc) => {
                messages.push({
                    user: doc.data().user,
                    msg: doc.data().msg,
                    count: doc.data().count
                });
            });
            this.setState({
                chatMessages: messages.sort((a, b) => {
                    return a.count - b.count;
                }),
                loading: false,
            });
        });
    }

    sendMessages = () => {
        const { msg, user, chatMessages } = this.state;
        // alert(JSON.stringify(user));
        this.ref.collection('chat').add({
            user: { email: user.email, uid: user.uid },
            msg,
            count: chatMessages.length + 1
        })
            .then((data) => {
                console.log(data);
            })
            .catch((error) => {
                this.setState({ loading: false })
            });
    }

    render() {
        const { chatMessages } = this.state;
        // console.log(chatMessages);
        return (
            <View style={{
                flex: 1,
                height: '100%',
                marginBottom: 50,
                justifyContent: 'flex-end'
            }}>
                <List containerStyle={{ marginBottom: 20, backgroundColor: '#fff', padding: 10, borderTopWidth: 0 }}>
                    <ScrollView
                        ref={ref => this.scrollView = ref}
                        onContentSizeChange={(contentWidth, contentHeight) => {
                            this.scrollView.scrollToEnd({ animated: true });
                        }}
                    >
                        {
                            chatMessages.map((item, i) => (
                                <View style={{ flexDirection: 'row', width: '100%' }} key={i}>
                                    <View>
                                        <Text style={{ fontSize: 15, fontWeight: 'bold', marginBottom: 5 }}>{item.msg}</Text>
                                        <Text style={{ fontSize: 12, color: '#d3d3d3' }}>{item.user.email}</Text>
                                    </View>
                                </View>
                            ))
                        }
                    </ScrollView>
                </List>

                <View style={styles.inputChat}>
                    <TextInput
                        style={styles.textinput}
                        placeholder='Digita logo'
                        onChangeText={
                            (text) => {
                                this.setState({ msg: text });
                            }
                        }
                    />
                    <Icon
                        name='send'
                        type='font-awesome'
                        color='#069'
                        containerStyle={styles.iconSend}
                        onPress={() => this.sendMessages()} />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: '100%'
    },
    button: {
        height: 45,
        backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    item: {
        backgroundColor: '#F5F5DC',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginBottom: 0,
        flexDirection: 'row'
    },
    flatstyle: {
        marginVertical: 60
    },
    textinput: {
        height: 45,
        backgroundColor: 'transparent',
        alignSelf: 'stretch',
        borderBottomColor: '#069',
        borderBottomWidth: 1,
        paddingHorizontal: 20,
        marginBottom: 10,
        width: '84%'
    },
    inputChat: {
        flexDirection: 'row',
        width: '100%'
    },
    iconSend: {
        width: '16%'
    }
});
