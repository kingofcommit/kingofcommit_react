import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator } from 'react-native';
import firebase from 'react-native-firebase';
import { Icon, Button } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            loading: false,
            email: 'carlos@gmail.com',
            password: '123456',
        });
        this.ref = firebase.firestore()
    }

    onLogin = () => {
        this.setState({ loading: true });
        const { email, password } = this.state;
        firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, password)
            .then((user) => {
                this.setState({ loading: false });
                this.props.navigation.navigate('Main');
            })
            .catch((error) => {
                this.setState({ loading: false });
                const { code, message } = error;
                Alert.alert(
                    code,
                    message,
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                )
            });
    }

    render() {
        return (
            <View style={{
                backgroundColor: this.state.background,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%'
            }}>
                {/* <Spinner visible={this.state.loading} textContent={"Organizando a cagada..."} textStyle={{ color: '#FFF' }} /> */}
                <TextInput
                    style={styles.textinput}
                    autoCapitalize='none'
                    placeholder='Email'
                    onChangeText={
                        (text) => {
                            this.setState({ email: text });
                        }
                    }
                />
                <TextInput
                    style={styles.textinput}
                    autoCapitalize='none'
                    placeholder='Senha'
                    onChangeText={
                        (text) => {
                            this.setState({ password: text });
                        }
                    }
                />
                <TouchableOpacity style={styles.button} onPress={this.onLogin} disabled={this.state.loading}>
                    {this.state.loading ?
                        <ActivityIndicator
                            size="small"
                            color="#00BFFF"
                            style={{ margin: 12 }}
                        /> : <Text style={styles.buttonText}>Login</Text>}
                </TouchableOpacity>
                <TouchableOpacity style={styles.subbutton} onPress={() => this.props.navigation.navigate('Register')} disabled={this.state.loading}>
                    <Text style={{ color: '#069', fontWeight: 'bold' }}>Register</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: '100%'
    },
    button: {
        height: 45,
        backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexDirection: 'row'
    },
    subbutton: {
        height: 45,
        // backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexDirection: 'row'
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    buttonGroup: {
        height: 45,
    },
    textinput: {
        height: 45,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        borderColor: '#EEE',
        borderWidth: 1,
        paddingHorizontal: 20,
        marginBottom: 10
    },
    item: {
        backgroundColor: '#F5F5DC',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginBottom: 0,
        flexDirection: 'row'
    },
    flatstyle: {
        marginVertical: 50
    }
});
