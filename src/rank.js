import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, FlatList, TextInput, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { List, ListItem, ButtonGroup, Icon, Button } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';

export default class RankScreen extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            rankTasks: [],
            loading: false,
            disabled: false,
            visible: false,
        });
        this.ref = firebase.firestore()
    }

    componentDidMount() {
        this.setState({ loading: true });
        this.unsubscribe = this.ref.collection('rank').onSnapshot((querySnapshot) => {
            const rank = [];
            querySnapshot.forEach((doc) => {
                rank.push({
                    user: doc.data().user,
                    uid: doc.data().uid,
                    count: doc.data().count,
                });
            });
            this.setState({
                rankTasks: rank.sort((a, b) => {
                    return (a.count < b.count);
                }),
                loading: false,
            });
        });
    }

    render() {
        const { rankTasks } = this.state;
        console.log(rankTasks);
        return (
            <LinearGradient colors={['#696969', '#696969']} style={{
                flex: 1,
                height: '100%',
                padding: 10,
                justifyContent: 'center'
            }}>
                <Spinner visible={this.state.loading} textContent={"Organizando a cagada..."} textStyle={{ color: '#FFF' }} />
                <List containerStyle={{ marginBottom: 20, backgroundColor: '#fff', padding: 10 }}>
                    {
                        rankTasks.map((item, i) => (
                            <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#696969' }} key={i}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: 25,
                                    textShadowColor: 'rgba(0, 0, 0, 0.75)',
                                    textShadowOffset: { width: -1, height: 5 },
                                    textShadowRadius: 10,
                                    backgroundColor: i == 0 ? '#FF4500' : i == 1 ? '#00BFFF' : 'green',
                                    width: 40,
                                    height: 40,
                                    textAlign: 'center',
                                    textAlignVertical: 'center',
                                    // borderRadius: 10,
                                    // borderWidth: 1,
                                    // borderColor: '#fff'
                                }}>{i + 1}</Text>
                                <View>
                                    <Text>{item.user}</Text>
                                    <Text>{item.count}</Text>
                                </View>
                            </View>
                        ))
                    }
                </List>
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: '100%'
    },
    button: {
        height: 45,
        backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    item: {
        backgroundColor: '#F5F5DC',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginBottom: 0,
        flexDirection: 'row'
    },
    flatstyle: {
        marginVertical: 60
    }
});
