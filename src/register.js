import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { Icon, Button } from 'react-native-elements';

export default class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            loading: false,
            email: '',
            password: '',
        });
        this.ref = firebase.firestore()
    }

    onRegister = () => {
        const { email, password } = this.state;
        firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password)
            .then((user) => {
                this.props.navigation.navigate('Main');
            })
            .catch((error) => {
                const { code, message } = error;
                console.log(code);
                console.log(message);
            });
    }

    render() {
        return (
            <View style={{
                backgroundColor: this.state.background,
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%'
            }}>
                <TextInput
                    style={styles.textinput}
                    autoCapitalize='none'
                    placeholder='Email'
                    onChangeText={
                        (text) => {
                            this.setState({ email: text });
                        }
                    }
                />
                <TextInput
                    style={styles.textinput}
                    autoCapitalize='none'
                    placeholder='Senha'
                    onChangeText={
                        (text) => {
                            this.setState({ password: text });
                        }
                    }
                />
                <TouchableOpacity style={styles.button} onPress={this.onRegister}>
                    <Text style={styles.buttonText}>Register</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.subbutton} onPress={() => this.props.navigation.goBack()} disabled={this.state.loading}>
                    <Text style={{ color: '#069', fontWeight: 'bold' }}>Login</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: '100%'
    },
    button: {
        height: 45,
        backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    subbutton: {
        height: 45,
        // backgroundColor: '#069',
        alignSelf: 'stretch',
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexDirection: 'row'
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    buttonGroup: {
        height: 45,
    },
    textinput: {
        height: 45,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        borderColor: '#EEE',
        borderWidth: 1,
        paddingHorizontal: 20,
        marginBottom: 10
    },
    item: {
        backgroundColor: '#F5F5DC',
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginBottom: 0,
        flexDirection: 'row'
    },
    flatstyle: {
        marginVertical: 50
    }
});
